﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityChan;

[RequireComponent(typeof(Animator))]
public class MotionTrigger : MonoBehaviour
{
	private Animator m_Animator = null;
	private MusicStarter m_MusicStarter = null;

	private Vector3 m_DefaultPos = Vector3.zero;
	private Quaternion m_DefaultRot = Quaternion.identity;

	private void Awake()
	{
		m_Animator = GetComponent<Animator>();
		m_MusicStarter = GetComponent<MusicStarter>();

		m_DefaultPos = transform.position;
		m_DefaultRot = transform.rotation;
	}

	private void Update()
	{
		if( Input.GetKeyDown(KeyCode.Space) )
		{
			m_Animator.SetTrigger( "Start" );
			//m_MusicStarter.OnCallMusicPlay( "start" );
		}

		if( Input.GetKeyDown(KeyCode.R) )
		{
			m_Animator.SetTrigger( "Reset" );
			m_MusicStarter.OnCallMusicPlay( "stop" );

			transform.position = m_DefaultPos;
			transform.rotation = m_DefaultRot;
		}
	}
}
